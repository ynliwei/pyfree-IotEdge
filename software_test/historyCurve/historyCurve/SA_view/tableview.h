#ifndef TABLEVIEW_HPP
#define TABLEVIEW_HPP

#include <QTableView>
// #include <QPoint>

#include "importconf.h"

QT_BEGIN_NAMESPACE
class QWidget;
class QAbstractItemModel;
class QContextMenuEvent;
class QMouseEvent;
// class QDragEnterEvent;
// class QDropEvent;
class QFile;
QT_END_NAMESPACE

class MyTableView : public QTableView
{
	Q_OBJECT
public:
    MyTableView(QAbstractItemModel * model,QWidget * parent = 0);
    ~MyTableView();

    QVector<QVector<QVariant> > getListComment();
protected:
    virtual void contextMenuEvent ( QContextMenuEvent * event ) = 0;
    // virtual void mousePressEvent ( QMouseEvent * event );
    // virtual void mouseReleaseEvent ( QMouseEvent * event );
    virtual void mouseMoveEvent ( QMouseEvent * event );
    // virtual void dragEnterEvent(QDragEnterEvent *event);
    // virtual void dropEvent(QDropEvent *event);
    virtual void colseExitListWidge();
    virtual void setSaveFilePath(QString *path);
protected:
    void defaultContextMenuEvent(QContextMenuEvent * event);
    void updateActions();
    bool insertColumn(QString _name,const QModelIndex &parent = QModelIndex(),int pos=-1);
    bool removeColumn(int pos,const QModelIndex &parent = QModelIndex());
    void importFileFromDir(int _dialogf=-1);
    void setFilePath(QString *path,int mode);
    bool isCorrectPath(QFile *file);
signals:

public slots:
    virtual void importFile();
    void importAsConf(ImportConfInfo inconf);
    virtual void saveFile();
    void saveAsCheck(QMap<int,bool> _list);
    virtual void sendColumShow(QMap<int,bool> _list);
protected slots:
    void recover();
    void insertChild();
    bool insertColumn(const QModelIndex &parent = QModelIndex(),int pos=-1);
    void insertRow();
    bool removeColumn(const QModelIndex &parent = QModelIndex(), int pos=-1);
    virtual void removeRow();
    virtual void removeRows();

    void CheckConfig(QString _str);
    virtual void itemClicked(const QModelIndex &index);

private:
    void win_init();
    void columCheck(int type);
protected:
    QModelIndex _currentIndex;//the QModelIndex from user mouseclicked check
private:
};

#endif // MYTREEVIEW_H
