#ifndef _CONF_ALARM_H_
#define _CONF_ALARM_H_
/***********************************************************************
  *Copyright 2020-03-06, pyfree
  *
  *File Name       : conf_alarm.h
  *File Mark       : 
  *Summary         : 告警策略配置
  *
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
#include <string>
#include <vector>

#include "dtypedef.h"
#include "conf_plan.h"

namespace pyfree
{
struct AlarmCMD
{
	unsigned long	ID;
	EventType		type;
	EventLevel		level;
	EventWay		way;
	std::string		time;
	std::string		desc;
	//设备编号
	unsigned long	devID;
	std::string		devDesc;
	unsigned long	pID;
	std::string     pDesc;
	//
	int dayAlarmCountLimit;	//每日告警次数限制
	int dayAlarmCount;		//每日告警次数
	int dayFlag;			//日期标识
	int alarmTime;			//已告警时间
	int alarmInterval;		//告警间隔,单位秒,默认60,取值范围[1,3600]
};

//告警分析计划
struct Alarm
{
	PlanProperty			planAtt;	//计划属性
	std::vector<PlanTime>	ptimes;		//定时集
	PlanSleep				psleep;		//定期属性
	PlanForStart			pFStart;	//启动计划
	std::vector<TVProperty> timeVS;		//时间区域集
	std::vector<PlanCondition> plancons;//数值条件集
	std::vector<AlarmCMD>	alarmCmds;	//告警信息集
};    
}
#endif
