#include "hashmap.h"

#ifdef WIN32
#include <Winsock2.h>
#else
#include <netinet/in.h>
#include <arpa/inet.h>
#endif
#include "pfunc.h"

KeyObj_FT::KeyObj_FT(int _gid, int _id, pyfree::PType _type)
	: m_gid(_gid), m_id(_id), m_type(_type)
{

};
//
int KeyObj_FT::cmp_Key(const KeyObj_FT &obj1, const KeyObj_FT &obj2)
{
	int diff = obj1.m_gid - obj2.m_gid;
	if (diff != 0) 		return diff;
	diff = static_cast<int>(obj1.m_type) - static_cast<int>(obj2.m_type);
	if (diff != 0) 		return diff;
	diff = obj1.m_id - obj2.m_id;
	if (diff != 0) 		return diff;
	return 0;
};

//////////////////////////////////////////////

KeyObj_TF::KeyObj_TF(pyfree::PType _pType, int _pID)
	: m_pType(_pType), m_pID(_pID)
{

};
//
int KeyObj_TF::cmp_Key(const KeyObj_TF &obj1, const KeyObj_TF &obj2)
{
	int diff = static_cast<int>(obj1.m_pType) - static_cast<int>(obj2.m_pType);
	if (diff != 0) 		return diff;
	diff = obj1.m_pID - obj2.m_pID;
	if (diff != 0) 		return diff;
	return 0;
};

////////////////////////////////////////
KeyObj_GAddr::KeyObj_GAddr(unsigned long long m_d_addr_, int m_addr_)
	: m_d_addr(m_d_addr_), m_addr(m_addr_), linkFlag(0)
{

};
//
long KeyObj_GAddr::cmp_Key(const KeyObj_GAddr &obj1, const KeyObj_GAddr &obj2)
{
	long diff = static_cast<long>(obj1.m_d_addr - obj2.m_d_addr);
	if (diff != 0) 		return diff;
	diff = obj1.m_addr - obj2.m_addr;
	if (diff != 0) 		return diff;
	return 0;
};
////////////////////////////////////////
KeyObj_GClient::KeyObj_GClient()
	: m_ip(pyfree::ipToInt("127.0.0.1")), m_port(810529), linkFlag(0)
{

};
//
KeyObj_GClient::KeyObj_GClient(unsigned long ip_, int port_)
	: m_ip(ip_), m_port(port_), linkFlag(0)
{

};
//
long KeyObj_GClient::cmp_Key(const KeyObj_GClient &obj1, const KeyObj_GClient &obj2)
{
	long diff = obj1.m_ip - obj2.m_ip;
	if (diff != 0) 		return diff;
	diff = obj1.m_port - obj2.m_port;
	if (diff != 0) 		return diff;
	return 0;
};
////////////////////////////////////////
KeyObj_Addr::KeyObj_Addr(int _addr, pyfree::PType _type)
	: m_addr(_addr), m_ptype(_type)
{

};
//
int KeyObj_Addr::cmp_Key(const KeyObj_Addr &obj1, const KeyObj_Addr &obj2)
{
	int diff = obj1.m_addr - obj2.m_addr;
	if (diff != 0) 		return diff;
	diff = static_cast<int>(obj1.m_ptype) - static_cast<int>(obj2.m_ptype);
	if (diff != 0) 		return diff;
	return 0;
};