/****************************************************************************
****************************************************************************/

#ifndef TREEMODEL_HPP
#define TREEMODEL_HPP

#include <QAbstractItemModel>
#include <QModelIndex>
#include <QVariant>

// #include "itemTypeIcon.hpp"

class TreeItem;

class TreeModel : public QAbstractItemModel
{
    Q_OBJECT
public:
    enum ViewType
    {
        SA_View = 1,
        GeneralTable = 99
    };
public:
    TreeModel(const QStringList &headers, ViewType viewType=GeneralTable, QObject *parent = 0);  
    ~TreeModel();

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const;

    QModelIndex index(int row, int column,
                      const QModelIndex &parent = QModelIndex()) const;
    QModelIndex parent(const QModelIndex &index) const;

    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;

    Qt::ItemFlags flags(const QModelIndex &index) const;
    bool setData(const QModelIndex &index, const QVariant &value,
                 int role = Qt::EditRole);
    bool setHeaderData(int section, Qt::Orientation orientation,
                       const QVariant &value, int role = Qt::EditRole);

public:
    QVariant getIconFlags(const QModelIndex &index) const;
    /*
    **insert a item to assign parent value,if be exit the same item, to ignore it
    **if the tree exit some items and their value equal to parent value,
    **insert new item to it's parent which is the fisrt item be finded .
    */
    bool insertItem(QVector<QVariant> Data,
      QVariant parent = QVariant()/*,QVariant itemType = ToIcon(), bool isCheck = false*/);
    /*
    **insert a item to assign parent tree struct,
    **if be exit the same item, to ignore it ,
    **if not find the parent node, create it.
    */
    bool insertItem(QStringList &parent, 
      QVector<QVariant> Data/*,QVariant itemType = ToIcon(), bool isCheck = false*/);
    //insert a item to assign parent,if be exit the same item, to ignore it
    void insertItemToParentBefore(const QModelIndex &index,QVector<QVariant> Data);
    void insertItemToParent(const QModelIndex &index,
      QVector<QVariant> Data/*,QVariant itemType = ToIcon() , bool isCheck = false*/);  
    void insertItemToParent(const QModelIndex &index,
      QVector< QVector<QVariant> > Datas/*,QVariant itemType = ToIcon() , bool isCheck = false*/,bool upf=true);   
    bool removeItem(const QVariant treeIndex);  
    //check the index to assign vlaue,if be exit, return thr false find  
    QModelIndex getModelIndex(const QVariant treeIndex);
    //get the index to assign parent and child's value
    QModelIndex indexChild(QVariant childTreeData,
      const QModelIndex &parent = QModelIndex()) const; 
    QModelIndex findIndex(QStringList &treeFalgs);
    QModelIndex typeCurrentIndexParent(const QModelIndex index, int &level) const;
    QVector<QModelIndex > findAllChild_layer(const QModelIndex &parent = QModelIndex());
    QVector<QModelIndex > findAllChild_order(const QModelIndex &parent = QModelIndex());

signals:

private:

    bool insertColumns(int position, int columns,
                       const QModelIndex &parent = QModelIndex());
    bool removeColumns(int position, int columns,
                       const QModelIndex &parent = QModelIndex());
    bool insertRows(int position, int rows,
                    const QModelIndex &parent = QModelIndex());
    bool removeRows(int position, int rows,
                    const QModelIndex &parent = QModelIndex()); 

    bool checkSwitch( const QModelIndex &index ) const; 
    void rptSetDeal(const QModelIndex &index,const QVariant &value);
private:
    TreeItem *getItem(const QModelIndex &index) const;
    ViewType _viewType;
    TreeItem *rootItem;
};

#endif
