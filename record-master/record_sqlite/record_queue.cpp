#include "record_queue.h"

#include "Log.h"

RecordQueue* RecordQueue::instance = NULL;
RecordQueue* RecordQueue::getInstance()
{
	if (NULL == RecordQueue::instance)
	{
		RecordQueue::instance = new RecordQueue();
	}
	return RecordQueue::instance;
}

void RecordQueue::Destroy()
{
	if (NULL != RecordQueue::instance) 
	{
		delete RecordQueue::instance;
		RecordQueue::instance = NULL;
	}
}

RecordQueue::~RecordQueue()
{

}
/////////////////////////////////////////////////////////
void RecordQueue::addItem(long devID,int _idx, long long _t, float _val)
{
	SADataItem _item;
	_item.Dev_Index = devID;
	_item.YC_Index = _idx;
	_item.Time = _t;
	_item.Value = _val;
	//_item.ToQT();
	data_mutex.Lock();
	std::map<long, std::list<SADataItem> >::iterator it = _datas.find(devID);
	if (it != _datas.end())
	{
		it->second.push_back(_item);
	}
	else {
		std::list<SADataItem> devPInfos;
		devPInfos.push_back(_item);
		_datas[devID] = devPInfos;
	}
	data_mutex.Unlock();
}

std::map<long, std::list<SADataItem> >  RecordQueue::getDatas()
{
	std::map<long, std::list<SADataItem> >  _wds;
	data_mutex.Lock();
	_wds = _datas;
	_datas.clear();
	data_mutex.Unlock();
	return _wds;
}

