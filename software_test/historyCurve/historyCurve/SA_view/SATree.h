#ifndef AGCTREE_H
#define AGCTREE_H

#include <QTreeView>
#include <QStringList>
#include <QVector>

QT_BEGIN_NAMESPACE
class QWidget;
class QAbstractItemModel;
class QContextMenuEvent;
class QMouseEvent;
QT_END_NAMESPACE

#include "agc_db/AGCDef.h"

class AGCTreeView : public QTreeView
{
	Q_OBJECT
public:
	AGCTreeView(QAbstractItemModel * model,QWidget * parent = 0);
	~AGCTreeView();

	void clear();
	void createAgcTree(QString _cz,QList<AGCDefItem> _agcdefs=QList<AGCDefItem>()
		,QList<AGCProtem> _agcpros=QList<AGCProtem>());
	void setModify(bool _f);
protected:
	void contextMenuEvent ( QContextMenuEvent * event );
	void mousePressEvent ( QMouseEvent * event );
    void mouseDoubleClickEvent ( QMouseEvent * event );
private:
	void win_init();
	void updateActions();
	void clearTree();
signals:
	void itemDoubleClick(QVector<DrawDef> _drawdefs);
	void saveAGCDefForNewFile(QList<AGCDefItem> cz_agcdef,QList<AGCProtem> cz_agcpro);
public slots:

private slots:
	void removeAGCDef();
	void removeRow();
	void createAgc();
	void createItem(AGCDefItem defItem);
	void createAgcPro();
	void createAgcProList(QList<AGCProtem> _agcpros);
	void saveAGCDef();
private:
	bool modify;
	QString cz_agc_path;
	QList<AGCDefItem> cz_agcdefs;
	QList<AGCProtem> cz_agcpros;
};

#endif //AGCTREE_H