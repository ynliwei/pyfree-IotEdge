#include "svc_control.h"

#include <stdio.h>

#include "datadef.h"
#include "pfunc_print.h"

#ifdef WIN32
//vs 对源码中中文字符的支持问题
#define status_unkown 	"unkown"
#define status_stop 	"off"
#define status_run 		"run"
#define status_pause 	"pause"
#else
#define status_unkown 	"未知态"
#define status_stop 	"已退出"
#define status_run 		"已运行"
#define status_pause 	"已暂停"
#endif // WIN32

void pyfree::svc_control(std::string svc_name, int cmd)
{
	switch (cmd)
	{
	case 1:
		SvcStop((char *)svc_name.c_str());
		break;
	case 2:
		SvcStart((char *)svc_name.c_str());
		break;
	case 3:
		break;
	default:
		break;
	}
	Print_NOTICE("svc_control %s=%d\n", svc_name.c_str(), cmd);
}

std::string pyfree::get_svc_status_desc(char *svc_name)
{
	std::string ret = "";
	int svc_state = 0;
	SvcQuery(svc_name, svc_state);
	switch (svc_state)
	{
	case SVC_UNKOWN_STATE:
	{
		ret = status_unkown;
	}
	break;
	case SVC_STOP:
	{
		ret = status_stop;
	}
	break;
	case SVC_RUN:
	{
		ret = status_run;
	}
	break;
	case SVC_PAUSE:
	{
		ret = status_pause;
	}
	break;
	default:
	{
		ret = status_unkown;
	}
	break;
	}
	return ret;
};
