#ifndef _CONF_PMAP_H_
#define _CONF_PMAP_H_

/***********************************************************************
  *Copyright 2020-03-06, pyfree
  *
  *File Name       : conf_pmap.h
  *File Mark       : 
  *Summary         : 采集点与转发点映射定义
  *
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/

#include "dtypedef.h"
namespace pyfree
{
struct PFrom
{
	int gid;			//采集端口编号
	int pid;			//采集点编号
	PType ptype;		//采集点类型
};
//对外的转发端口针对每种通信只有一个端口
struct PTo
{
	int pid;			//转发点编号
	PType ptype;		//转发点类型
	float value;		//转发点数值
};

struct PMap
{
	PFrom pfrom;
	PTo pto;
};
};
#endif // PMAP_CONF_H