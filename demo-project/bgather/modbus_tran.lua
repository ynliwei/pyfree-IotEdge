﻿--应用脚本
--脚本语言:LUA (http://www.lua.org )
--项目应用:pyfree项目-模拟modbus从站端通信
--脚本版本:1.0
-----------------------------------------------------------------------------------------------------------------------------------------
function Split(szFullString, szSeparator)  
	local nFindStartIndex = 1  
	local nSplitIndex = 1  
	local nSplitArray = {}  
	while true do  
		local nFindLastIndex = string.find(szFullString, szSeparator, nFindStartIndex)  
		if not nFindLastIndex then  
			nSplitArray[nSplitIndex] = string.sub(szFullString, nFindStartIndex, string.len(szFullString))  
			break  
		end  
		nSplitArray[nSplitIndex] = string.sub(szFullString, nFindStartIndex, nFindLastIndex - 1)  
		nFindStartIndex = nFindLastIndex + string.len(szSeparator)  
		nSplitIndex = nSplitIndex + 1  
	end  
	return nSplitArray  
end  

function getTotalCallCMD()
	local allcall = ""
	return allcall,0
end

function getDownControl(exetype, ptype, addr, val)
	--collectgarbage("collect")
	local controlCmd = "NULL"
	local check = 1
	
	if 1==exetype then
		controlCmd = "01030000000AC5CD"
		check = 0
	else
		if 1==ptype then
			controlCmd = "0106"..string.format("%04X",addr)..string.format("%04X",val)
		elseif 2==ptype then
			controlCmd = "0106"..string.format("%04X",addr)..string.format("%04X",val)
		else
			return controlCmd,check
		end
	end

	return controlCmd,check
end
--目前仅在串口转发时调用
--pinfos_desc格式"id,val;id,val;..."
function getResponse(pinfos_desc,resp_type,task_id)
	--print(pinfos_desc)
	local list = Split(pinfos_desc, ";")
	local recs = ""
	local size = #list
	if size==0 then
		return recs,0
	end
	if size==1 then
		local list_p = Split(list[1], ",")
		if #list_p==2 then
			if 1==resp_type then
				recs = "0103"..string.format("%04X",list_p[1])..string.format("%04X",list_p[2])
				return recs,1
			elseif 2==resp_type then
				recs = "0106"..string.format("%04X",list_p[1])..string.format("%04X",list_p[2])
				return recs,1
			else
				return recs,0
			end
		end
	end
	--print(size)
	recs = "0103"..string.format("%02X",2*size)
	for i=1,size do
		--print(list[i])
		local list_p = Split(list[i], ",")
		if #list_p==2 then
			recs = recs..string.format("%04X",list_p[2])
		end
	end
	local check = 1
	--print(recs)
	return recs,check
end
--返回信息点描述[点编号,数值,点类型,请求类型,任务编号]
--点编号=0时为总召请求
function getReciveIDVAL(up_cmd,down_cmd)
	--print(up_cmd)
	local recs = ""
	local size = 0
	if string.len(up_cmd)<16 then
		return recs,size
	end
	if string.sub( up_cmd, 3, 12 )=="030000000A" then
		recs = string.format("%d,%d,%d,%d,%d",0,0,1,1,0)
		size = 1
		return recs,size
	end
	if string.sub( up_cmd, 3, 4 )=="06" or string.sub( up_cmd, 3, 4 )=="03" then
		local hid = string.sub( up_cmd, 5, 6 )
		local lid = string.sub( up_cmd, 7, 8 )
		local id = tonumber("0X"..hid)*256+tonumber("0X"..lid)
		
		local hval = string.sub( up_cmd, 9, 10 )
		local lval = string.sub( up_cmd, 11, 12 )
		local val = tonumber("0X"..hval)*256+tonumber("0X"..lval)
		if string.sub( up_cmd, 3, 4 )=="06" then
			recs = string.format("%d,%d,%d,%d,%d",id,val,1,2,0)
		else
			recs = string.format("%d,%d,%d,%d,%d",id,val,1,1,0)
		end
		--print(recs)
		return recs,1
	end
	return recs,size
end
