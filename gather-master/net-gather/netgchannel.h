#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _NET_G_CHANNEL_H_
#define _NET_G_CHANNEL_H_
/***********************************************************************
  *Copyright 2020-03-06, pyfree
  *
  *File Name       : netgchannel.h
  *File Mark       : 
  *Summary         : 
  *网口采集，作为客户端连接到第三方服务进行sokcet通信读写数据,通过lua脚本进行编解码
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/

#include "lib_acl.h"
#include "acl_cpp/lib_acl.hpp"

#include "gchannel.h"

class SocketClinet;

class NetGChannel : public GatherChannel,public acl::thread
{
public:
/**
	 * 构造函数
     * @param gid_ {int} 采集口编号
	 * @return { } 
	 */
    NetGChannel(int gid_);
    ~NetGChannel();

    void* run();

    /**
	 * 上行接口调用本采集信道的下控执行函数,重写父类的函数
	 * @param exetype {int} 执行类型{1,查询;2,设值}
	 * @param type {PType} 信息点类型{遥信/遥测等}
	 * @param pid {int} 信息点编号
	 * @param val {float} 值
	 * @param task_id {ulong} 任务编号
	 * @return {void } 
	 */
    void downControl(int _exetype, int _type, int _pid, float _val, unsigned long _taskID=0);
private:
    /**
	 * 本实例初始化
     * @param gid_ {int} 采集口编号
	 * @return {void } 
	 */
    void init(int gid_);
    /**
	 * 对来自采集口的数据进行帧解析和报文解析
	 * @param buf {char*} 报文指针
	 * @param len {int} 报文长度
	 * @return {int } <0时异常
	 */
    int AddFrame(const unsigned char *buf, int len);	                            //帧解析
    /**
	 * 对来自采集网口的报文进行解析
	 * @param buf {char*} 报文指针
	 * @param len {int} 报文长度
     * @param dev_addr {ulong} 设备地址,对于本接口，由于是sokcet客户端,默认是1
	 * @return {int } <0时异常
	 */
    void msgResponse(unsigned char *buf, int len, unsigned long dev_addr=1);		//返回指令解析
    /**
	 * 获取下发给采集口的业务协议报文
	 * @param buf {char*} 报文指针
	 * @return {int } 报文长度
	 */
    int getBuffer(unsigned char* _buf);
private:
    bool running;
    pyfree::NetPort netargs;
    SocketClinet* ptr_client;
};

#endif //