#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _QUEUE_DATA_SINGLE_H_
#define _QUEUE_DATA_SINGLE_H_

/***********************************************************************
  *Copyright 2020-03-06, pyfree
  *
  *File Name       : queuedata_single.h
  *File Mark       : 
  *Summary         : 
  *单体类数据队列,线程安全,具体实现在其父类QueueData<T>
  *
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
#include "queuedata.h"

template <class T>
class QueueDataSingle : public QueueData<T>
{
public:
	static QueueDataSingle* getInstance();
	static void Destroy();
	~QueueDataSingle();

	void setQueueDesc(std::string desc);
private:
	QueueDataSingle(std::string desc="single_thread_queue") 
		: QueueData<T>(desc)
	{
	};
	QueueDataSingle& operator=(const QueueDataSingle&) {return this;};
private:
	static QueueDataSingle* instance;
};

template <class T>
QueueDataSingle<T>* QueueDataSingle<T>::instance = NULL;

template <class T>
QueueDataSingle<T>* QueueDataSingle<T>::getInstance()
{
	if (NULL == QueueDataSingle::instance)
	{
		QueueDataSingle::instance = new QueueDataSingle();
	}
	return QueueDataSingle::instance;
}

template <class T>
void QueueDataSingle<T>::Destroy()
{
	if (NULL != QueueDataSingle::instance) 
	{
		delete QueueDataSingle::instance;
		QueueDataSingle::instance = NULL;
	}
}

template <class T>
QueueDataSingle<T>::~QueueDataSingle()
{

}

template <class T>
void QueueDataSingle<T>::setQueueDesc(std::string desc)
{
	this->queue_desc = desc;
}

#endif //_QUEUE_DATA_SINGLE_H_
