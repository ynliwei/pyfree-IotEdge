#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _PFUNC_TIME_H_
#define _PFUNC_TIME_H_
/***********************************************************************
  *Copyright 2020-03-06, pyfree
  *
  *File Name       : pfunc_time.h
  *File Mark       : 
  *Summary         : 时间处理相关的函数集
  *
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/

#include <string>

namespace pyfree
{
/**
	 * 获取当前时间的字符串表述
	 * @return {string } 当前时间,格式为yyyy-MM-dd hh:mm:ss(年-月-日 时:分:秒)
	 */
	std::string getCurrentTime();
	/**
	 * 根据当前时间的得到任务编号
	 * @param index {int} 当前时间内多个任务时为了冲突设置的索引号
	 * @return {unsigned long } 与当前时间为参标的任务编号
	 */
	unsigned long  getTaskIDFromDateTime(int index=0);
	/**
	 * 获取当前时间的字符串描述,可以指定表述格式
	 * @param format {string} 时间表述格式
	 * @param localf {bool} 是否本地时间
	 * @return {string } 字符串描述时间
	 */
	std::string getCurrentTimeByFormat(std::string format="%04d-%02d-%02d %02d:%02d:%02d",bool localf=true);
	/**
	 * 获取当前时间的秒表述
	 * @param localf {bool} 是否本地时间
	 * @return {uint } 当前时间的秒
	 */
	unsigned int  getCurDayTime( bool localf = true);
	/**
	 * 获取当天的字符串描述,可以指定表述格式
	 * @param format {string} 时间表述格式
	 * @param localf {bool} 是否本地时间
	 * @return {string } 字符串描述时间
	 */
	std::string getCurDayStr(std::string format = "%04d-%02d-%02d", bool localf = true);
	/**
	 * 获取当前时间的毫秒表述
	 * @param localf {bool} 是否本地时间
	 * @return {uint } 当前时间的毫秒
	 */
	unsigned int getUsec();
	/**
	 * 获取程序启动计时时间
	 * @param localf {bool} 是否本地时间
	 * @return {uint } 计时时间
	 */
	unsigned long long getClockTime();
	/**
	 * 获取时间描述字符串
	 * @param sec {uint } 秒
	 * @param msec {int } 毫秒,=-1时,抛弃毫秒格式
	 * @return {string} 时间描述字符串
	 */
	std::string getDateTime(unsigned int sec, int msec=-1);
	/**
	 * 获取当前时间信息,输出秒和毫秒
	 * @param sec {uint& } 秒
	 * @param msec {uint& } 毫秒
	 * @return {void}
	 */
	void getCurTime(unsigned int &s, unsigned int &ms);
	/**
	 * 获取当前时间信息,返回毫秒
	 * @return {ulong}
	 */
	unsigned long getCurMsTime();
	/**
	 * 当前时间(一天内)是否在匹配范围内
	 * @param startTime {uint } 开始范围,距凌晨多少分钟
	 * @param endTime {uint } 结束范围,距凌晨多少分钟
	 * @return {bool}
	 */
	bool isMapTime(unsigned int startTime,unsigned int endTime);
};

#endif 
