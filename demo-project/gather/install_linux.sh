#!/bin/bash
#@pyfree 2020-04-16,py8105@163.com
#创建程序目录
#run for root
#定义程序全局路径变量
#程序根目录
dir='/usr/local/gather'
curdir=$(pwd)

if [ ! -d $dir ]; then
    mkdir -p $dir
fi
#
echo "appdir="$dir
echo "curdir="$curdir
#复制程序文件到程序目录
/bin/cp -f {pyfree-gather,SWL,*.xml,*.lua} $dir
#
ls $dir
#
if [ ! -f "$dir/gatherStart.sh" ];then
#创建程序启动脚本gather
cat > $dir/gatherStart.sh << eof
#!/bin/bash
pid=\`ps -ef | grep "pyfree-gather*" | grep -v "grep" | wc -l\`
if [ \$pid -eq 0 ];then
    cd $dir
	echo "The gather server will be start"
    nohup ./pyfree-gather & > /dev/null
else
    echo "The gather server is alreadly running"
fi 
eof
fi

if [ ! -f "$dir/gatherStop.sh" ];then
#创建程序关闭脚本gather
cat > $dir/gatherStop.sh << eof
#!/bin/bash
pid=\`ps -ef | grep "pyfree-gather*" | grep -v "grep" | wc -l\`
if [ \$pid -ne 0 ];then
	echo "The gather server will be stop"
    killall -9 pyfree-gather
else
	echo "The gather server is stoping, don't stop it"
fi 
eof
fi

#给服务启动脚本添加执行权限
chmod o+x $dir/{gatherStart.sh,gatherStop.sh}

if [ ! -f "/usr/lib/systemd/system/pyfreeGather.service" ];then
#创建程序启动文件到centos7服务启动路径/usr/lib/systemd/system
cat > /usr/lib/systemd/system/pyfreeGather.service <<eof
[Unit]
Description=pyfree-gather
After=syslog.target network.target remote-fs.target nss-lookup.target
[Service]
Type=forking
ExecStart=$dir/gatherStart.sh
ExecReload=
ExecStop=$dir/gatherStop.sh
PrivateTmp=true
[Install]
WantedBy=multi-user.target
eof
fi

#给gather生成liscense文件sn.txt, 如果是32位系统，修改为SWL_x32.exe 
#0表示网卡，1表示磁盘； 22表示生成sn.txt文件，这里选择网卡，生成sn.txt liscense文件
cd $dir
./SWL 0 22
cd $curdir

#设置开机启动gather服务
systemctl daemon-reload
chmod o-x /usr/lib/systemd/system/pyfreeGather.service
systemctl enable pyfreeGather.service
#查看服务是否开机启动：
#systemctl is-enabled pyfreeGather.service
#设置开机时禁用服务
#systemctl disable pyfreeGather.service
#启动gather服务
#systemctl start pyfreeGather.service

#停止pcsserver服务
#systemctl stop pyfreeGather.service
