#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _SOCKET_PRIVATE_ACL_H_
#define _SOCKET_PRIVATE_ACL_H_
/***********************************************************************
  *Copyright 2020-03-06, pyfree
  *
  *File Name       : socket_private_acl.h
  *File Mark       : 
  *Summary         : 
  *基于第三方库(acl)开发的TCP-Socket通信接口,提供了信道的连接、关闭、接入侦听、数据读写等操作
  *连接客户端将加入Map容器进行标记,方便针对各个连接独立处理
  *
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
/*
TCP-SOcket服务处置类
*/
#include <map>
#include <queue>
#include "Mutex.h"
#include "hashmap.h"
#include "datadef.h"

#include "acl_cpp/lib_acl.hpp"


struct ClientSOCKET_ACL
{
	acl::socket_stream* client;
	int flag;
};

class SocketPrivate_ACL
{
public:
	/* 构造函数
     * @param ip_ {string} 
	 * @param port {uint} 
	 * @return { } 
	 */
	SocketPrivate_ACL(unsigned int port,std::string ip_="0.0.0.0");
	~SocketPrivate_ACL();
public:
	int onConnect();
	void disConnect();
	void close_client(acl::socket_stream* client_acl);
	bool emptyClients();
	bool getNewAddClient(std::vector<unsigned long> &ips);
	int Read(std::map<KeyObj_GClient, RDClient> &bufs);
	int Write(const char* buf, int size);
	int Write(unsigned long ipInt, const char* buf, int size);

	bool Accept();

	//void routeLinkInfo();
	void addLinkFlag(KeyObj_GClient it);
	bool pop(KeyObj_GClient &it);
	void routClientLinkState();
private:
	acl::server_socket		m_SSocket;			//服务端
	acl::string				addr;
	unsigned int			m_Port;				//端口变量
	bool					m_OnListen;			//用于标注侦听
	PYMutex					m_MyMutex;
	std::map<KeyObj_GClient, ClientSOCKET_ACL>		m_CSockets;			//绑定客户端
	bool					newlinkF;			//新链接标记,为降低巡检而设
	PYMutex							m_MyMutexLink;
	std::queue<KeyObj_GClient>		m_CSocketLinks;		//客户端链接标记
	unsigned int					m_RoutCheck_UI;		//校对是否有客户端处于链接状态
};

#endif
