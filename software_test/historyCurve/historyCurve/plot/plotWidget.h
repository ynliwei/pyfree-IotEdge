#ifndef PLOTWIDGET_H
#define PLOTWIDGET_H

#include <QWidget>
#include <QList>
#include <QMap>
#include <QDateTime>

#include "qwt/qwt_plot.h"
// #include "qwt/qwt_plot_curve.h"
// #include "qwt/qwt_plot_marker.h"
#include "qwt/qwt_plot_curve.h"
#include "qwt/qwt_plot_histogram.h"
#include "qwt/qwt_symbol.h"
#include "dataitem.h"

#define MY_M_PI       3.14159265358979323846

QT_BEGIN_NAMESPACE
class QwtInterval;
class QwtPlotMarker;
class MyQwtPlotZoomer;
class QwtPlotPanner;
class TimeScaleDraw;
class DoubleScaleDraw;
class MyQwtPlotPicker;
class Cuvre;
class MyHistogram;
QT_END_NAMESPACE

struct BaseView
{
    int _tf;
    QDate _td;
    QTime _tt;
    qreal _valx;
    qreal _val;
    qreal _with;
    qreal _height;
};

class PlotWidget : public QwtPlot
{
    Q_OBJECT
public:
    explicit PlotWidget( QWidget *parent = NULL );
    explicit PlotWidget( const QwtText &title, QWidget *parent = NULL );
    ~PlotWidget();

    void drawHistogram(QString _title,QMap<QString,int> datas);
    void drawItemsDatas(QString _title,QMap<QDateTime,qreal> itemdatas);
    void drawItemsDatas(QMap<QString,QMap<QDateTime,qreal> > itemdatas);
    void clear();
public slots:
    void drawItemsData(DataItem itemdata);
    void drawItemsDatas(QList<DataItem> itemdatas);
    void resetBackground(QColor _col);
    void resetCrossColor(QColor _col);
    void resetItemColor(QColor _col);
    void resetItemDrawStyle(QString _sty);
private slots:
    void resetcanvas();
private:
    void init();
    void updateGradient(const QColor _col);
    void initgrid();
    void initcanvas();
    void initaxis();
    void initLegend();
    void initzoom();
    void initmov();
    void initPicker();
    void initMagn();
    void initScaleXY(bool tf=true);
    void IntervalInit(double xMin, double xMax, double yMin, double yMax);
    void setInterval(double xMin, double xMax, double yMin, double yMax);

    void makerInit(double xValue, QString xmark,double yValue, QString ymark);
    void initYMarker(double yValue, QString mark);
    void initXMarker(double xValue, QString mark);
    void setXMarker(double xValue, QString xmark = "");
    void setYMarker(double yValue, QString ymark = "");
    void AddXMarker(double xValue, QString mark);
    void AddYMarker(double yValue, QString mark);

    void setSamples();

    int findCurve(const QString title);
    int findHistogram(const QString title);

    //create curve and the title is only marker
    void createNewCurve(QString title,QwtPlotCurve::CurveStyle style,QColor _col = QColor(Qt::blue));
    void removeCurve(QString title);
    void clearCurve();
        //Curve pro set
    void setCurveStyle(QString title,QwtPlotCurve::CurveStyle style);
    void setCurveColor(QString title,QColor color);
    void setCurveBaseline(QString title, double basline);
    void setCurveBrush(QString title, QBrush brush);
    void setCurvePen(QString title, QPen pen);

    //CurveSymbol pro set
    void setCurveSymbolStyle(QString title,
                        QwtSymbol::Style style);
    void setCurveSymbolBrush(QString title,
                        QBrush brush);
    void setCurveSymbolPen(QString title,
                        QPen pen);
    void setCurveSymbolSize(QString title,
                        QSize size);

    //
    void createNewHistogram(QString title,QwtPlotHistogram::HistogramStyle style);
    //
    void removeHistogram(QString title);
    //
    void addDataToHistogram(QString title, const QwtIntervalSample val, const QColor color);
    void addDataToHistogram(int curveFlag, const QwtIntervalSample val, const QColor color);
    void clearHistogram();

    void setPlotTilte(QString Title);
    void setPlotFooter(QString footer);
    void setAxisTitleX(QString xTitle);
    void setAxisTitleY(QString yTitle);
//    void setData(QVector< QPointF > &data);

    void addPoint(QString title,QPointF point);
    void addData(QString title,const double x, const double y);
    void addData(int curveFlag,const double x, const double y);
    void addCurveInterval(QString title, const int from, const int to);
    void setCurveMove( QString title, const double x_Move, const double y_Move);

    void update();

    bool autoUpdate();
    void setAutoUpdate(bool flags);

    void getAxisTimeVal(QDateTime _st, QDateTime _et, QDateTime &_tval,  qint64 &_xtInt, qint64 &_xIVal);
    void getAxisTimeVal(QTime _st, QTime _et, QTime &_tval, int &_xtInt, int &_xIVal);
    // void setMouseMoveButton(int  button, int  buttonState = Qt::NoButton);
    void getAxisVal(qreal _min, qreal _max, qreal &_dval, qreal &_yval);
    int qrealMaxPos(qreal _val);

    void setBaseView();
private:
    // QString _curveName;
    // QwtInterval *d_intervalX;
    // QwtInterval *d_intervalY;
    QwtPlotMarker *m_QwtPlotMarkerX;
    QwtPlotMarker *m_QwtPlotMarkerY;

    MyQwtPlotZoomer *d_zoomer;
    QwtPlotPanner *d_panner;
    TimeScaleDraw *m_TimeScaleDraw;
    DoubleScaleDraw *m_DoubleScaleDrawX;
    DoubleScaleDraw *m_DoubleScaleDraw;
    MyQwtPlotPicker *m_MyQwtPlotPicker;

    QList<Cuvre*> *m_Cuvre;
    QwtPlotCurve::CurveStyle _style;
    QList<MyHistogram*> *m_MyHistogram;

    BaseView _bv;

    QVector<QColor> lineColor;
};

#endif // PLOTWIDGET_H
