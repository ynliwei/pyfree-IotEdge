#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _SOCKET_GATHER_H_
#define _SOCKET_GATHER_H_
/***********************************************************************
  *Copyright 2020-03-06, pyfree
  *
  *File Name       : socket_gather.h
  *File Mark       : 
  *Summary         : 建立socket服务端,承载下级采控服务的链接与数据通信,实现采集级联
  *
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/

#include <queue>
#include "Mutex.h"
#include "datadef.h"
#include "hashmap.h"

#include "gchannel.h"

class SocketPrivate_ACL;
class SocketService;
class SocketGahterRead;
class SocketGatherWrite;

class SocketGather : public GatherChannel
{
public:
	SocketGather(int gid);
	~SocketGather(void);
public:

	int Read(){ return -1; };
	int Write(){ return -1; };
	//
	//int Read(char* buf, int size);
	int Write(const char* buf, int size);

  	//////////////////////////////////////////////////////////
	/**
	 * 对来自采集口的数据进行帧处理和报文解析
	 * @param link {ulong} 采集设备标识,本实例为整型网络地址,已标识来自不同客户端的连接
	 * @param buf {char*} 报文指针
	 * @param len {int} 报文长度
	 * @return {int } <0时异常
	 */
	int AddFrame(unsigned long link, const unsigned char *buf, int len);
	/**
	 * 对来自采集网口的报文进行解析
	 * @param buf {char*} 报文指针
	 * @param len {int} 报文长度
     * @param dev_addr {ulong} 设备地址,对于本接口，为各个客户端的整型网络地址
	 * @return {int } <0时异常
	 */
	void msgResponse(unsigned char *buf, int len, unsigned long dev_addr);		//返回指令解析
	/////////////////////////////////////////////////////////
	/**
	 * 上行接口调用本采集信道的下控执行函数,重写父类的函数
	 * @param exetype {int} 执行类型{1,查询;2,设值}
	 * @param type {PType} 信息点类型{遥信/遥测等}
	 * @param pid {int} 信息点编号
	 * @param val {float} 值
	 * @param task_id {ulong} 任务编号
	 * @return {void } 
	 */
	void downControl(int exetype, int type, int pid, float val, unsigned long task_id=0);
	/**
	 * 获取下发给采集口的业务协议报文
	 * @param ipInt {ulong} 为各个客户端的整型网络地址
	 * @param buf {char*} 报文指针
	 * @return {int } 报文长度
	 */
	int getBuffer(unsigned long &ipInt, unsigned char* _buf);
	//////////////////////////////////////////////////////////
	/**
	 * 采集口各个信息点的数据更新上报,主要作为代理函数调用服务类的更新函数,以便其他类调用
	 * @return {void } 
	 */
	void TimeUp();
	/**
	 * 设置采集口下各个客户端的通信状态
	 * @param ipInt {ulong} 为各个客户端的整型网络地址
	 * @return {void } 
	 */
	void setLink(unsigned long ipInt, const bool flag);
	/**
	 * 获取本tcp-sokcet服务的监听网口地址
	 * @return {string } 
	 */
	std::string ip_addr();
	/**
	 * 获取本tcp-sokcet服务的监听端口
	 * @return {int } 
	 */
	int port();
	/**
	 * 获取本tcp-sokcet服务帧处理方式
	 * @return {int } 
	 */
	int type();
	/**
	 * 将从采集通道读取的数据加入缓存队列
	 * @param it {ChannelCmd} 读取数据
	 * @return {void }
	 */
	void add(ChannelCmd it);
	/**
	 * 从缓存队列获取来自采集端的头元素
	 * @param it {ChannelCmd} 读取数据
	 * @return {void }
	 */
	bool getFirst(ChannelCmd &it);
	/**
	 * 从缓存队列删除头元素
	 * @return {void }
	 */
	bool removeFirst();
	/**
	 * 从缓存队列获取来自采集端的头元素,并将其从队列删除
	 * @param it {ChannelCmd} 读取数据
	 * @return {void }
	 */
	bool pop(ChannelCmd &it);
private:
  	pyfree::NetPort m_NetArg;	//网络配置信息
	SocketPrivate_ACL *socket_acl;
	// SocketGatherData *gsd_ptr;
	SocketService *socket_srv;
	SocketGahterRead *socket_read;
	SocketGatherWrite *socket_write;
};

#endif //_MYSOCKET_H_
