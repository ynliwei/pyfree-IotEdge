#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _PFUNC_FORMAT_H_
#define _PFUNC_FORMAT_H_
/***********************************************************************
  *Copyright 2020-03-06, pyfree
  *
  *File Name       : pfunc_format.h
  *File Mark       : 
  *Summary         : 字符格式转换相关的函数集
  *
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
#include <string>

namespace pyfree
{
	/**
	 * 字符格式转换,hex to ascii
	 * @param pSrc {const char* } 源字符串
	 * @param pDst {unsigned char*} 转换结果
	 * @param nSrcLength {int} 源字符串长度
	 * @return {int} 转换结果长度
	 */
	int string2bytes(const char* pSrc, unsigned char* pDst, int nSrcLength);
	/**
	 * 字符格式转换,ascii to hex
	 * @param pSrc {const unsigned char* } 源字符串
	 * @param pDst {char*} 转换结果
	 * @param nSrcLength {int} 源字符串长度
	 * @return {int} 转换结果长度
	 */
	int bytes2string(const unsigned char* pSrc, char* pDst, int nSrcLength);
	/**
	 * hex字符 转 ascii字符
	 * @param data_hex {uchar } hex字符
	 * @return {char} ascii字符
	 */
	char HexToASCII(unsigned char data_hex);
	/**
	 * hex字符串 转 ascii字符串
	 * @param OutStrBuffer {char* } ascii字符串
	 * @param InHexBuffer {uchar* } hex字符串
	 * @param HexLength {uint } hex字符串大小
	 * @return {void}
	 */
	void HexGroupToString(char* OutStrBuffer, unsigned char* InHexBuffer,unsigned int HexLength);
	/**
	 * ascii字符串 转 hex字符串
	 * @param OutHexBuffer {uchar* } hex字符串
	 * @param InStrBuffer {char* } ascii字符串
	 * @param strLength {uint } ascii字符串大小
	 * @return {void}
	 */
	bool StringToHexGroup(unsigned char *OutHexBuffer, char *InStrBuffer, unsigned int strLength);
	/**
	 * from UNICODE
	 * @param pSrc {const char* } 
	 * @param pDst {char* } 
	 * @param nSrcLength {uint } 字符串大小
	 * @return {void}
	 */
	int invernumber(const char* pSrc,char* pDst,int nSrcLength);
	/**
	 * to UNICODE
	 * @param pSrc {const char* } 
	 * @param pDst {char* } 
	 * @param nSrcLength {uint } 字符串大小
	 * @return {void}
	 */
	int encodeucs2(const char* pSrc, unsigned char* pDst, int nSrcLength);
    /**
	 * Hex转换
	 * @param x {uchar} 转换字符
	 * @return {uchar } 转换结果
	 */
	unsigned char ToHex(unsigned char x);
	/**
	 * hex转换
	 * @param x {uchar} 转换字符
	 * @return {uchar } 转换结果
	 */
	unsigned char FromHex(unsigned char x);
	/**
	 * url转换
	 * @param str {string} 转换字符串
	 * @return {string } 转换结果
	 */
	std::string UrlEncode(const std::string& str);
	/**
	 * url转换
	 * @param str {string} 转换字符串
	 * @return {string } 转换结果
	 */
	std::string UrlDecode(const std::string& str);
	/**
	 * to hex16
	 * @param in {char* } 源字符串
	 * @param out {uchar* } 目标字符串
	 * @param size {int } 字符串大小
	 * @return {void}
	 */
	void strToHex16(char *in,unsigned char *out,int size);
	/**
	 * from hex16
	 * @param in {char* } 源字符串
	 * @param out {uchar* } 目标字符串
	 * @param size {int } 字符串大小
	 * @return {void}
	 */
	void strFromHex16(unsigned char *in,char *out,int size);
	/**
	 * to base64字符
	 * @param c6 {char } acsii字符
	 * @return {char} base64编码
	 */
	char ConvertToBase64(char c6);
	/**
	 * to base64字符串
	 * @param dbuf {char* } 目标字符串
	 * @param buf128 {char* } 源字符串
	 * @param len {int } 字符长度
	 * @return {void} 
	 */
	void EncodeBase64(char *dbuf, char *buf128, int len);
};

#endif
