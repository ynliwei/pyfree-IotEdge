#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _COMSUMER_ALIYUN_H_
#define _COMSUMER_ALIYUN_H_

/***********************************************************************
  *Copyright 2020-03-06, pyfree
  *
  *File Name       : cunsumer_aliyun.h
  *File Mark       :
  *Summary         : 阿里云物联网的订购线程实现
  *
  *
  *Current Version : 1.00
  *Author          : PengYong
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/

#include "lib_acl.h"
#include "acl_cpp/lib_acl.hpp"

#include "datadef.h"
#include "queuedata_single.h"

class BusinessDef;
class VerifyForControlCache;

class ConsumerAliyun : public acl::thread
{
public:
	ConsumerAliyun(std::map<std::string, AliyunDeviceMaps> subTopicMaps_);
	virtual ~ConsumerAliyun();
	//
	void* run();
private:
	//禁止拷贝构造和赋值运算
	ConsumerAliyun(const ConsumerAliyun&);
	ConsumerAliyun& operator=(const ConsumerAliyun&) { return *this; };
private:
	void receive();
	bool createControlCmd(unsigned long long devID, unsigned int pID, float val);
private:
	bool running;
	std::map<std::string, AliyunDeviceMaps> subTopicMaps;
	BusinessDef *ptr_CacheDataObj;
	bool is_Verification;
	VerifyForControlCache *ptr_vcc;
	QueueDataSingle<DataToGather> *to_gather_queue;
	QueueDataSingle<RCacheAliyun> *cache_from_aliyun_queue;
};


#endif
