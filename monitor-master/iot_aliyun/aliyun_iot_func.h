#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _ALIYUN_IOT_FUNC_H_
#define _ALIYUN_IOT_FUNC_H_
/***********************************************************************
  *Copyright 2020-03-06, pyfree
  *
  *File Name       : aliyun_iot_func.h
  *File Mark       :
  *Summary         : 阿里云物联网接口相关宏和回调函数定义
  *
  *
  *Current Version : 1.00
  *Author          : PengYong
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
#include "iot_import.h"
#include "iot_export.h"

#define  PAYLOAD_FORMAT					"{\"id\":\"%lld\",\"version\":\"1.0\",\"method\":\"%s\",\"params\":%s}"
#define  PAYLOAD_FORMAT_TAG				"{\"id\":\"%lld\",\"version\":\"1.0\",\"method\":\"%s\",\"params\":[%s]}"
#define  ALINK_METHOD_PROP_POST			"thing.event.property.post"
#define  ALINK_METHOD_EVENT_POST		"thing.event.ControlFail.post"
#define  ALINK_METHOD_TAG_UPDATE		"thing.deviceinfo.update"
#define  ALINK_METHOD_TAG_DELETE		"thing.deviceinfo.delete"
#define  ALINK_METHOD_DESIRED_GET		"thing.property.desired.get"
#define  ALINK_METHOD_DESIRED_DELETE	"thing.property.desired.delete"
#define  ALINK_METHOD_DISABLE			"thing.disable"
#define  ALINK_METHOD_ENABLE			"thing.enable"
#define  ALINK_METHOD_EVENT_UP			"thing.event.%s.post"

#define ALINK_COMMENT_FORMAT				"clientId%s&%sdeviceName%sproductKey%stimestamp%lld"
//login
#define ALINK_TOPIC_DEV_LOGIN				"/ext/session/%s/%s/combine/login"
#define ALINK_TOPIC_DEV_LOGIN_REPLY			"/ext/session/%s/%s/combine/login_reply"
#define ALINK_TOPIC_DEV_LOGIN_REPLY_FLAG	"combine/login_reply"
//logout
#define ALINK_TOPIC_DEV_LOGOUT				"/ext/session/%s/%s/combine/logout"
#define ALINK_TOPIC_DEV_LOGOUT_REPLY		"/ext/session/%s/%s/combine/logout_reply"
#define ALINK_TOPIC_DEV_LOGOUT_REPLY_FLAG	"combine/logout_reply"
//post
#define ALINK_TOPIC_EVENT_PRO_POST			"/sys/%s/%s/thing/event/property/post"
#define ALINK_TOPIC_EVENT_PRO_POST_REPLY	"/sys/%s/%s/thing/event/property/post_reply"
#define ALINK_TOPIC_EVENT_PRO_POST_REPLY_FLAG	"thing/event/property/post_reply"
//set
#define ALINK_TOPIC_SERVICE_PRO_SET         "/sys/%s/%s/thing/service/property/set"
#define ALINK_TOPIC_SERVICE_PRO_SET_REPLY	"/sys/%s/%s/thing/service/property/set_reply"
#define ALINK_TOPIC_SERVICE_PRO_SET_FLAG	"thing/service/property/set"
//tag update
#define ALINK_TOPIC_TAG_UPDATE				"/sys/%s/%s/thing/deviceinfo/update"
#define ALINK_TOPIC_TAG_UPDATE_REPLY		"/sys/%s/%s/thing/deviceinfo/update_reply"
#define ALINK_TOPIC_TAG_UPDATE_REPLY_FLAG	"thing/deviceinfo/update_reply"
//tag delete
#define ALINK_TOPIC_TAG_DELETE				"/sys/%s/%s/thing/deviceinfo/delete"
#define ALINK_TOPIC_TAG_DELETE_REPLY		"/sys/%s/%s/thing/deviceinfo/delete_reply"
#define ALINK_TOPIC_TAG_DELETE_REPLY_FLAG	"thing/deviceinfo/delete_reply"
//desired get
#define ALINK_TOPIC_DESIRED_GET				"/sys/%s/%s/thing/property/desired/get"
#define ALINK_TOPIC_DESIRED_GET_REPLY		"/sys/%s/%s/thing/property/desired/get_reply"
//desired delete
#define ALINK_TOPIC_DESIRED_DELETE			"/sys/%s/%s/thing/property/desired/delete"
#define ALINK_TOPIC_DESIRED_DELETE_REPLY	"/sys/%s/%s/thing/property/desired/delete_reply"
//disable
#define ALINK_TOPIC_DISABLE					"/sys/%s/%s/thing/disable"
#define ALINK_TOPIC_DISABLE_REPLY			"/sys/%s/%s/thing/disable_reply"
//enable
#define ALINK_TOPIC_ENABLE					"/sys/%s/%s/thing/enable"
#define ALINK_TOPIC_ENABLE_REPLY			"/sys/%s/%s/thing/enable_reply"
//client up event
#define ALINK_TOPIC_EVENT_UP				"/sys/%s/%s/thing/event/%s/post"
#define ALINK_TOPIC_EVENT_UP_REPLY			"/sys/%s/%s/thing/event/%s/post_reply"
//server down event
#define ALINK_TOPIC_EVENT_DOWN				"/sys/%s/%s/thing/service/%s"
#define ALINK_TOPIC_EVENT_DOWN_REPLY		"/sys/%s/%s/thing/service/%s_reply"

#define login_format		\
"{\"id\":\"%lld\",\"params\":{ \"productKey\":\"%s\",\"deviceName\":\"%s\",\"clientId\":\"%s&%s\",\"timestamp\":\"%lld\",\"signMethod\":\"hmacSha1\",\"sign\":\"%s\",\"cleanSession\":\"true\"}}"
#define logout_format \
"{\"id\": \"%lld\",\"params\" : {\"productKey\": \"%s\",\"deviceName\" : \"%s\"}}"
#define add_dev_format		\
"{\"id\":\"%lld\",\"version\":\"1.0\",\"params\":[{\"deviceName\":\"%s\",\"productKey\":\"%s\",\"sign\":\"%s\",\"signmethod\":\"hmacSha1\",\"timestamp\":\"%lld\",\"clientId\":\"%d\"}],\"method\":\"thing.topo.add\"}"
#define MQTT_MSGLEN             (1024)

#define EXAMPLE_TRACE(fmt, ...)  \
    do { \
        HAL_Printf("%s|%03d :: ", __func__, __LINE__); \
        HAL_Printf(fmt, ##__VA_ARGS__); \
        HAL_Printf("%s", "\r\n"); \
    } while(0)
/*
*阿里云物联网平台mqtt通信的默认回调函数
*/
void event_handle(void *pcontext, void *pclient, iotx_mqtt_event_msg_pt msg);
/*
*从json消息中获取code_id
*/
void getReplyCode(const char* payload, int &code_id);
/*
*阿里云物联网平台mqtt通信的login回调函数
*/
void login_message_arrive(void *pcontext, void *pclient, iotx_mqtt_event_msg_pt msg);
/*
*阿里云物联网平台mqtt通信的logout回调函数
*/
void logout_message_arrive(void *pcontext, void *pclient, iotx_mqtt_event_msg_pt msg);
/*
*阿里云物联网平台mqtt通信的push回调函数
*/
void push_reply_message_arrive(void *pcontext, void *pclient, iotx_mqtt_event_msg_pt msg);
/*
*将来自阿里云物联网平台的消息加入缓存队列
*/
void service_set_message_arrive(void *pcontext, void *pclient, iotx_mqtt_event_msg_pt msg);
/*
*阿里云物联网平台mqtt通信的tag_up回调函数
*/
void tag_update_reply_arrive(void *pcontext, void *pclient, iotx_mqtt_event_msg_pt msg);
/*
*阿里云物联网平台mqtt通信的tag_del回调函数
*/
void tag_del_reply_arrive(void *pcontext, void *pclient, iotx_mqtt_event_msg_pt msg);
/*
*阿里云物联网平台mqtt通信的desired_get回调函数
*/
void desired_get_reply_arrive(void *pcontext, void *pclient, iotx_mqtt_event_msg_pt msg);
/*
*阿里云物联网平台mqtt通信的desired_del回调函数
*/
void desired_del_reply_arrive(void *pcontext, void *pclient, iotx_mqtt_event_msg_pt msg);
/*
*阿里云物联网平台mqtt通信的事件更新回调函数
*/
void event_up_reply_arrive(void *pcontext, void *pclient, iotx_mqtt_event_msg_pt msg);
/*
*阿里云物联网平台mqtt通信的base回调函数
*/
void base_reply_arrive(void *pcontext, void *pclient, iotx_mqtt_event_msg_pt msg);
//阿里云物联网平台mqtt消息打印输出函数
void printf_out_topic_info(iotx_mqtt_topic_info_pt ptopic_info);
#endif
