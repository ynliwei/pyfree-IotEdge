#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _SOCKET_LOCAL_H_
#define _SOCKET_LOCAL_H_
/***********************************************************************
  *Copyright 2020-03-06, pyfree
  *
  *File Name       : socket_loacl.h
  *File Mark       : 
  *Summary         : 
  *1)建立socket服务线程,监听服务端口,实现数据通信
  *2)数据读线程,读取来自本地可视化终端的下发数据
  *3)数据写入线程,将设备状态推送给本地可视化终端
  *
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
#include <string>

class SocketPrivate_ACL;
class SocketSrv_ACL;
class SocketLocalRead;
class SocketLocalWrite;

class SocketLocal
{
public:
	SocketLocal(std::string ip,unsigned int port);
	virtual ~SocketLocal(void);
public:

	virtual int Read(){ return -1; };
	virtual int Write(){ return -1; };
//protected:
	//int Read(char* buf, int size);
	int Write(const char* buf, int size);
private:
	SocketPrivate_ACL   *socket_acl;
	SocketSrv_ACL		    *socket_srv_acl;
	SocketLocalRead     *socket_l_read;
	SocketLocalWrite    *socket_l_write;
};

#endif //_MYSOCKET_H_
