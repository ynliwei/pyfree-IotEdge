/*
**
*/
#include "SATable.h"

#include <QApplication>
#include <QModelIndex>
#include <QMenu>
#include <QAction>
#include <QColor>
#include <QPalette>
#include <QHeaderView>
#include <QAbstractItemModel>
#include <QMouseEvent>
#include <QDebug>

#include "treemodel.h"
#include "plot/pfunc_qt.h"
// #include "FileQt.h"

SATableView::SATableView(QAbstractItemModel * model,QWidget * parent)
    : MyTableView(model,parent)
{
    win_init();
}

SATableView::~SATableView()
{

};

void SATableView::clear()
{
    this->recover();
};

void SATableView::createDataList(QList<SADataItem> _datas)
{
    TreeModel *model = qobject_cast<TreeModel *>(this->model());
    QVector< QVector<QVariant> > Datas;
    for (QList<SADataItem>::iterator it = _datas.begin();
            it != _datas.end(); it++)
    {
        // QString key = QString("%1-%2").arg((*it).Dev_Index).arg((*it).YC_Index);
        // QString vKey = (*it).dt_STR.leftRef(13).toString();

        // QMap<QString,QMap<QString,CVal> >::iterator itc=cvals.find(key);
        // if(itc!=cvals.end()){
        //     QMap<QString,CVal>::iterator itv = itc.value().find(vKey);
        //     if(itv != itc.value().end()){
        //         cvals[key][vKey].value+=(*it).Value;
        //         cvals[key][vKey].size+=1;
        //         if(cvals[key][vKey].maxVal<(*it).Value)
        //             cvals[key][vKey].maxVal=(*it).Value;
        //     }else{
        //         CVal cv;
        //         cv.value = (*it).Value;
        //         cv.size = 1;
        //         cv.maxVal = (*it).Value;
        //         cvals[key][vKey] = cv;
        //     }
        // }else{
        //     CVal cv;
        //     cv.value = (*it).Value;
        //     cv.size = 1;
        //     cv.maxVal = (*it).Value;
        //     QMap<QString,CVal> cvs;
        //     cvs[vKey] = cv;
        //     cvals[key]=cvs;
        // }

        QVector<QVariant> Data;
        Data << (*it).Dev_Index
            << (*it).YC_Index
            << (*it).dt_STR
            << (*it).val_STR;
        Datas << Data;
        if (Datas.size()>=100)
        {
            model->insertItemToParent(QModelIndex(),Datas);
            Datas.clear();
        }
    }
    if(!Datas.isEmpty())
        model->insertItemToParent(QModelIndex(),Datas);

    // FileQt *m_FileQt = FileQt::getInstance();

    // for(QMap<QString,QMap<QString,CVal> >::iterator itc = cvals.begin(); itc!=cvals.end(); itc++){

    //     QVector<QString> list;
    //     for(QMap<QString,CVal>::iterator itv = itc.value().begin(); itv!=itc.value().end();itv++){
            
    //     qDebug()<<QString("%1,%2,%3,%4,%5,%6")
    //         .arg(itc.key()).arg(itv.key())
    //         .arg(itv.value().value).arg(itv.value().size)
    //         .arg(itv.value().value/itv.value().size)
    //         .arg(itv.value().maxVal);
            
    //         list << QString("%1,%2,%3,%4,%5\r\n")
    //         .arg(itv.key())
    //         .arg(itv.value().value).arg(itv.value().size)
    //         .arg(itv.value().value/itv.value().size)
    //         .arg(itv.value().maxVal); 
    //     }
    //     m_FileQt->writeListInfo(list,itc.key());
    // }
};

void SATableView::addDataToList(QList<SADataItem> _datas)
{
    TreeModel *model = qobject_cast<TreeModel *>(this->model());
    for (QList<SADataItem>::iterator it = _datas.begin();
            it != _datas.end(); it++)
    {
        QVector<QVariant> Data;
        Data << (*it).Dev_Index 
            << (*it).YC_Index 
            << (*it).dt_STR
            << (*it).val_STR;
        model->insertItemToParentBefore(QModelIndex(),Data);
    }

}

void SATableView::win_init()
{
    verticalHeader()->setDefaultSectionSize(36);
   // this->sortByColumn(0,Qt::AscendingOrder);
    // this->header()->setResizeMode(QHeaderView::Stretch);
    // this->header()->setResizeMode(QHeaderView::Interactive);
    // this->header()->setCascadingSectionResizes(true);
    // this->header()->setResizeMode(2,QHeaderView::ResizeToContents);
    // this->header()->resizeSection(2,30);
    // this->header()->setUpdatesEnabled(false);
    // this->header()->setUpdatesEnabled(true);
    // this->setUpdatesEnabled(false);
    //this->header()->setCascadingSectionResizes(true);
    // this->header()->setStretchLastSection(true);
    //this->setUpdatesEnabled(true);
   // this->setItemsExpandable (true);
    
    // this->setMouseTracking(true);
    // this->setTextElideMode(Qt::ElideRight);//...display at the end of the text.
    this->setSelectionBehavior(QAbstractItemView::SelectRows);
    // this->setSelectionMode(QAbstractItemView::ExtendedSelection);
    // this->setEditTriggers(QAbstractItemView::NoEditTriggers);
    this->setAlternatingRowColors(true);
    // this->setCornerButtonEnabled(true);
    // this->setSortingEnabled(true);

    this->setColumnWidth(0,80);
    this->setColumnWidth(1,80);
    this->setColumnWidth(2,240);
    this->setColumnWidth(3,80);

    // this->setDragDropMode(QAbstractItemView::DragOnly);
}

void SATableView::contextMenuEvent ( QContextMenuEvent * event )
{
    defaultContextMenuEvent(event);
}

void SATableView::saveFile()
{
    MyTableView::saveFile();
}