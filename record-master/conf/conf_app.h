#ifndef _CONF_APP_H_
#define _CONF_APP_H_
/***********************************************************************
  *Copyright 2020-03-06, pyfree
  *
  *File Name       : conf_app.h
  *File Mark       : 
  *Summary         : 程序配置信息
  *
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
#include <string>
#include <map>
#include <vector>

namespace pyfree
{

struct SysAssistConf
{
	SysAssistConf()
	: diskSymbol('D')
    , freeSizeLimit(10000)
    , dayForLimit(7)
    , gLogDir("log")
    , recordFunc(false)
    , gDataDir("gadata")
    , m_MonRecord_b(0)
	{

	};
	char diskSymbol;		//信息存储盘符
	int freeSizeLimit;		//存储路径所在磁盘剩余空间(MB)
	int dayForLimit;		//日志保留最低天数
	std::string gLogDir;	//日志输出目录
    bool recordFunc;		//数据记录功能开关
	std::string gDataDir;	//数据存储目录
	int m_MonRecord_b;
};

struct UdpIoConf
{
    UdpIoConf()
        : local_ip("127.0.0.1")
        , local_port(60004)
    {

    };
    std::string local_ip;
    int local_port;
};

struct AreaInfo
{
	AreaInfo()
	: areaId(1)
    , areaType(1)
    , areaName("zhsye")
    , areaDesc("珠海金鼎")
	{

	};
	int areaId;					//区域编号
	int areaType;				//区域类型,暂无使用
	std::string areaName;		//区域类型,暂无使用
	std::string areaDesc;		//区域描述
};


//后续需要重构,将各个功能模块涉及参数结构化,并配置文件(xml)里作为一个子节点集写入
struct AppConf
{
	AppConf()	
	{

	};
    //系统辅助功能配置
    SysAssistConf saconf;   //
    //
    UdpIoConf udpconf;
	//area info
    AreaInfo ainfo;             //
};
};

#endif